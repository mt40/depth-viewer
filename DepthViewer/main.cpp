#include "opencv2/core/utility.hpp"
#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

const string window_name = "Depth by Scale";
const int slider_max = 100;
int slider_value;

Mat depthMap, dst;

/**
 * @invD inverse depth
 * @scale larger = more difference. Must be from 0 to 1. Set to
 * 0 for normal behavior.
 */
Vec3b getVisualizationColor(float invD, float scale = 0) {
	if (invD < scale) {
		return cv::Vec3b(255, 255, 255); // very far, white color
	}

	invD = (invD - scale) / (1 - scale);

	float r = abs(0 - invD) * 255;
	float g = abs(1 - invD) * 255;
	float b = abs(2 - invD) * 255;

	uchar rc = min(255.0f, r);
	uchar gc = min(255.0f, g);
	uchar bc = min(255.0f, b);

	return cv::Vec3b(255 - rc, 255 - gc, 255 - bc);
}

void process(float scale) {
	int height = depthMap.rows, width = depthMap.cols;

	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			float depth = depthMap.at<float>(y, x);
			dst.at<Vec3b>(y, x) = getVisualizationColor(depth, scale);
		}
	}
}

/**
 * Callback for trackbar
 */
void on_trackbar(int, void*)
{
	float scale = (float)slider_value / slider_max;
	process(scale);

	imshow(window_name, dst);
}

int main(int argc, char** argv)
{
	String path = "C:/Users/mt/Desktop/DepthViewer/depth_data/depth.yml";
	FileStorage fs(path, FileStorage::READ);
	fs["depth"] >> depthMap;
	fs.release();

	if (!depthMap.data) { 
		printf("Error loading data\n");
		return -1; 
	}

	slider_value = 0;

	// Create Windows
	namedWindow(window_name, CV_WINDOW_AUTOSIZE);

	dst = Mat(depthMap.rows, depthMap.cols, CV_8UC3);
	imshow(window_name, dst);

	// Create Trackbars
	char TrackbarName[50];
	sprintf(TrackbarName, "Scale*%d", slider_max);

	createTrackbar(TrackbarName, window_name, &slider_value, slider_max, on_trackbar);

	// Show some stuff
	on_trackbar(slider_value, 0);

	// Wait until user press some key
	waitKey(0);
	return 0;
}